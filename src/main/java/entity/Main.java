package entity;

import javax.persistence.*;

public class Main {
    public static void main(String[] args) {
        Customer cust = new Customer();
        cust.setId(9);
        cust.setFirstName("Test");
        cust.setLastName("Romriell");
        cust.setEmail("testromriell@gmail.com");


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(cust);
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}
